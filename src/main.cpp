//
// Created by holcombea on 4/19/2023.
//

#include "object.h"
#include <iostream>
#include <string>

using namespace std;

int main() {

    Map* map = new Map();
    Player* player;
    bool isDone;
    for (int i = 0; i < 5 && !isDone; ++i) {
        for (int j = 0; j < 5 && !isDone; ++j) {
            if (map->getCell(j, i)->getToken() == '-') {
                player = new Player(map, j, i);
                isDone = true;
                cout << "Location: " << endl;
                cout << "X: " << j << endl;
                cout << "Y: " << i << endl;
            }
        }
    }
    map->write(false);
    char move;

    isDone = false;
    bool debugMode = false;
    do {
        if(player->getMap()->getCell(player->xLocation, player->yLocation)->getToken() != 'B'){
            player->checkSurroundingSpots();
        }
        cout << "Action: N)orth, S)outh, E)ast, W)est, shoot A)rrow, T)oggle DebugMode, Q)uit: ";
        cin >> move;
        cout << endl;
        switch (tolower(move)) {
            case 'n':
                isDone = player->move('n');
                break;
            case 'e':
                isDone = player->move('e');
                break;
            case 's':
                isDone = player->move('s');
                break;
            case 'w':
                isDone = player->move('w');
                break;
            case 't':
                debugMode = !debugMode;
                break;
            case 'q':
                isDone = true;
                break;
            case 'a':
                if(player->getNumArrows() > 0) {
                    char shootDirection;
                    cout << "Action: shoot N)orth, S)outh, E)ast, W)est: ";
                    cin >> shootDirection;
                    cout << endl;
                    switch (tolower(shootDirection)) {
                        case 'n':
                            isDone = player->shootArrow('n');
                            break;
                        case 'e':
                            isDone = player->shootArrow('e');
                            break;
                        case 's':
                            isDone = player->shootArrow('s');
                            break;
                        case 'w':
                            isDone = player->shootArrow('w');
                            break;
                        default:
                            cout << "Invalid Option, try again." << endl;
                    }
                } else {
                    std::cout << "No arrows left" << endl;
                }
                break;
            default:
                cout << "Invalid Option, try again." << endl;
                break;
        }
        map->write(debugMode);
    } while (!isDone);
}