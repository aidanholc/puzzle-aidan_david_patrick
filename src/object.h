#ifndef PUZZLE_AIDAN_DAVID_PATRICK_MAP_H
#define PUZZLE_AIDAN_DAVID_PATRICK_MAP_H

class MapCell;
class CaveObject;
class Map;
class Player;

class MapCell {
private:
    int xLocation, yLocation;
    bool hasPlayer;
    CaveObject* caveObject;
public:
    MapCell(int x, int y, CaveObject* object);
    char getToken();
    void enter();
    void vacate();
    void setCaveObject(CaveObject* c);
    CaveObject* getCaveObject();
};

class Map{
private:
    const static int WIDTH = 5;
    const static int HEIGHT = 5;
    bool debugMode;
    MapCell* cells[WIDTH][HEIGHT];
public:
    // initialize empty map
    Map();
    // read the map from cin
    void load();
    // write the full map to cout
    void write(bool debugMode);

    MapCell* getCell(int x, int y);
};

class Player {

public:
    int xLocation;
    int yLocation;
    Map* map;
    int numArrows;



    Player(Map* newMap, int x, int y);
    bool move(char direction);
    void setPosition(int newX, int newY);
    //returns the current number of arrows after one is shot in specified direction
    bool shootArrow(char direction);
    void targetSquareLocation(int& targetX, int& targetY, char direction);
    void incrementArrow();
    void checkSurroundingSpots();
    Map* getMap();
    int getNumArrows();
};

class CaveObject {
public:
    virtual void describe() = 0;
    virtual bool interactWithPlayer(Player* p) = 0;
    virtual char getToken() = 0;
    virtual bool hitByArrow() = 0;
};

class Chungus: public CaveObject{
    char getToken() override;
    bool interactWithPlayer(Player* player) override;
    void describe() override;
    bool hitByArrow() override;
};

class Bat: public CaveObject {
    char getToken() override;
    bool interactWithPlayer(Player* player) override;
    void describe() override;
    bool hitByArrow() override;
};

class Pit: public CaveObject{
    char getToken() override;
    bool interactWithPlayer(Player* player) override;
    void describe() override;
    bool hitByArrow() override;
};

class Arrow: public CaveObject{
    char getToken() override;
    bool interactWithPlayer(Player* player) override;
    void describe() override;
    bool hitByArrow() override;
};

class Ground: public CaveObject{    char getToken() override;
    bool interactWithPlayer(Player* player) override;
    void describe() override;
    bool hitByArrow() override;
};

#endif