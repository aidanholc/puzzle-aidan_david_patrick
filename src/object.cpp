//
// Created by holcombea on 4/26/2023.
//

#include "object.h"
#include <string>
#include <iostream>
#include <random>
#include <algorithm>

using namespace std;

char Chungus::getToken() {
    return 'C';
}

void Chungus::describe() {
    cout << "You smell a terrible stench." << endl;
}

bool Chungus::interactWithPlayer(Player *player) {
    cout << "The Chungus ate you whole! You Lose!" << endl;
    return true;
}

bool Chungus::hitByArrow() {
    cout << "you shot the Chungus! You Win!" << endl;
    return true;
}


char Pit::getToken() {
    return 'P';
}

void Pit::describe() {
    cout << "You feel a cold breeze." << endl;
}

bool Pit::interactWithPlayer(Player *player) {
    cout << "You fell down the bottomless pit! You Lose!" << endl;
    return true;
}

bool Pit::hitByArrow() {
    cout << "you shot into the pit!" << endl;
    return false;
}

char Bat::getToken() {
    return 'B';
}

void Bat::describe() {
    cout << "You hear the sound of wings." << endl;
}

bool Bat::interactWithPlayer(Player *player) {
    cout << "Bats carried you away!" << endl;
    srand(time(nullptr));
    int x = rand() % 4;
    int y = rand() % 4;
    player -> setPosition(x, y);
    player->getMap()->getCell(x, y)-> getCaveObject()->interactWithPlayer(player);
    return false;
}

bool Bat::hitByArrow() {
    cout << "you shot into the bats, they lived though!" << endl;
    return false;
}

char Arrow::getToken() {
    return 'A';
}

void Arrow::describe() {
    cout << "You see an arrow on the ground." << endl;
}

bool Arrow::interactWithPlayer(Player *player) {
    cout << "You collected an arrow!" << endl;
    player->incrementArrow();
    player->getMap()->getCell
    (player->xLocation, player->yLocation)->setCaveObject(new Ground());
    return false;
}

bool Arrow::hitByArrow() {
    cout << "you shot into another arrow! nothing happened" << endl;
    return false;
}

char Ground::getToken() {
    return '-';
}

void Ground::describe() {}

bool Ground::interactWithPlayer(Player *player) {
    return false;
}

bool Ground::hitByArrow() {
    cout << "you shot into the ground, nothing happened!" << endl;
    return false;
}


MapCell::MapCell(int x, int y, CaveObject* caveObject) {
    this->xLocation = x;
    this->yLocation = y;
    this->caveObject = caveObject;
    this->hasPlayer = false;
}

void MapCell::setCaveObject(CaveObject *c) {
    caveObject = c;
}

char MapCell::getToken() {
    if(hasPlayer) {
        return '!';
    }
    return caveObject->getToken();
};

void MapCell::enter() {
    hasPlayer = true;

}

void MapCell::vacate() {
    hasPlayer = false;
}

CaveObject* MapCell::getCaveObject() {
    return caveObject;
}

Map::Map() {
    this->load();
}

void Map::load() {
    // create a way of placing random elements on the board
    vector<int> typeChooser;
    for (int i = 0; i < 25; ++i) typeChooser.push_back(i);

    // used random shuffle but that's deprecated now so this is what it told me to swap to
    shuffle(typeChooser.begin(), typeChooser.end(), std::mt19937(std::random_device()()));

    for(int y = 0; y < HEIGHT; ++y) {
        for(int x = 0; x < WIDTH; ++x) {
            int index = WIDTH * y  + x;
            int type = typeChooser.at(index);
            if(type < 2) {  // include 2 bats
                cells[x][y] = new MapCell(x, y, new Bat());
            } else if (type < 4) { // include 2 pits
                cells[x][y] = new MapCell(x, y, new Pit());
            } else if (type < 8) { // include 4 arrows
                cells[x][y] = new MapCell(x, y, new Arrow());
            } else if (type < 9) { // include 1 chungus
                cells[x][y] = new MapCell(x, y, new Chungus());
            } else { // for all others make them blank
                cells[x][y] = new MapCell(x, y, new Ground());
            }
        }
    }
}

void Map::write(bool debugMode) {
    if(debugMode) {
        std::cout << "+-----+" << std::endl;

        for (int y = 0; y < HEIGHT; ++y) {
            std::cout << "|";
            for (int x = 0; x < WIDTH; ++x) {
                std::cout << cells[x][y]->getToken();
            }
            std::cout << "|" << std::endl;
        }
        std::cout << "+-----+" << std::endl;
    } else {
        std::cout << "+-----+" << std::endl;

        for (int y = 0; y < HEIGHT; ++y) {
            std::cout << "|";
            for (int x = 0; x < WIDTH; ++x) {
                if(cells[x][y]->getToken() != '!'){
                    std::cout << "-";
                } else {
                    std::cout << "!";
                }
            }
            std::cout << "|" << std::endl;
        }
        std::cout << "+-----+" << std::endl;
    }
}

MapCell *Map::getCell(int x, int y) {
    return cells[x][y];
}

Player::Player(Map *newMap, int x, int y) {
    map = newMap;
    xLocation = x;
    yLocation = y;
    numArrows = 5;
    map->getCell(x, y)->enter();
}

int Player::getNumArrows() {
    return numArrows;
}

Map* Player::getMap() {
    return map;
}

bool Player::move(char direction){
    int targetX = xLocation;
    int targetY = yLocation;

    targetSquareLocation(targetX, targetY, direction);
    setPosition(targetX, targetY);
    std::cout << "y: " << yLocation << ", x: " << xLocation << std::endl;

    CaveObject *objAtTarget = map->getCell(targetX, targetY)->getCaveObject();
    bool isEnd = objAtTarget->interactWithPlayer(this);
    if (objAtTarget->getToken() == '!') {
        std::cout << "You win the game";
    }
    return isEnd;
}

void Player::checkSurroundingSpots() {
    int targetX = xLocation;
    int targetY = yLocation;
    targetSquareLocation(targetX, targetY, 'n');
    map->getCell(targetX, targetY)->getCaveObject()->describe();

    targetX = xLocation;
    targetY = yLocation;
    targetSquareLocation(targetX, targetY, 's');
    map->getCell(targetX, targetY)->getCaveObject()->describe();

    targetX = xLocation;
    targetY = yLocation;
    targetSquareLocation(targetX, targetY, 'e');
    map->getCell(targetX, targetY)->getCaveObject()->describe();

    targetX = xLocation;
    targetY = yLocation;
    targetSquareLocation(targetX, targetY, 'w');
    map->getCell(targetX, targetY)->getCaveObject()->describe();


}

bool Player::shootArrow(char direction) {
    numArrows = numArrows - 1;
    int targetX = xLocation;
    int targetY = yLocation;

    targetSquareLocation(targetX, targetY, direction);

    CaveObject *objAtTarget = map->getCell(targetX, targetY)->getCaveObject();
    bool isDone = objAtTarget->hitByArrow();
    std::cout << "Num Arrows Left: " << numArrows << std::endl;
    return isDone;


}

void Player::incrementArrow() {
    numArrows = numArrows + 1;
}


void Player::setPosition(int x, int y) {
    map->getCell(x, y)->enter();
    map->getCell(xLocation, yLocation)->vacate();
    xLocation = x;
    yLocation = y;
}

void Player::targetSquareLocation(int &targetX, int &targetY, char direction) {

    if (direction == 'e') {
        targetX = targetX + 1;
    } else if (direction == 'w') {
        targetX = targetX - 1;
    } else if (direction == 'n') {
        targetY = targetY - 1;
    } else if (direction == 's') {
        targetY = targetY + 1;
    }

    //loop back to other side of map if shooting on the boundary
    if (targetX == 5) {
        targetX = 0;
    } else if (targetX == -1) {
        targetX = 4;
    } else if (targetY == -1) {
        targetY = 4;
    } else if (targetY == 5) {
        targetY = 0;
    }
}